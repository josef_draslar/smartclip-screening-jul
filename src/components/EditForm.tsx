import React, { RefObject, useEffect, useRef, useState } from "react";
import Input from "./form/Input";
import Form from "./form/Form";
import Button from "./form/Button";
import { ContactInterface } from "./ContactList";

interface Props {
  contactToBeChanged: ContactInterface;
  onSubmit: (contact: ContactInterface) => void;
  focusFirstInputOnLoad?: boolean;
}

const EditForm = ({
  contactToBeChanged,
  onSubmit,
  focusFirstInputOnLoad,
}: Props) => {
  const [contact, setContact] = useState(contactToBeChanged);
  const onChange = ({
    target: { name, value },
  }: React.ChangeEvent<HTMLInputElement>) => {
    setContact({ ...contact, [name]: value });
  };

  const firstInputRef: RefObject<HTMLInputElement> =
    useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (focusFirstInputOnLoad) {
      firstInputRef?.current?.focus();
    }
  }, [focusFirstInputOnLoad, firstInputRef]);

  return (
    <Form onSubmit={() => onSubmit(contact)}>
      <Input
        name="name"
        onChange={onChange}
        value={contact.name}
        ref={firstInputRef}
      />
      <Input name="phone" onChange={onChange} value={contact.phone} />
      <Button type="submit" disabled={!contact.name || !contact.phone}>
        Save
      </Button>
    </Form>
  );
};

export default EditForm;
