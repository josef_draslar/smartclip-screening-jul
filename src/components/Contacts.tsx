import React, { useState } from "react";
import "./Contacts.css";
import ContactList, { ContactInterface } from "./ContactList";
import { defaultContacts } from "../utils/defaultContacts";
import Button from "./form/Button";
import AddContactModal from "./AddContactModal";
import EditContactModal from "./EditContactModal";

const Contacts = () => {
  const [data, setData] = useState<ContactInterface[]>(defaultContacts);
  const [contactToBeChanged, setContactToBeChanged] =
    useState<ContactInterface | null>(null);
  const [addContactModalOpened, setAddContactModalOpened] =
    useState<boolean>(false);

  const editRow = (contact: ContactInterface) => {
    setContactToBeChanged(contact);
  };
  const saveRow = (contact: ContactInterface) => {
    if (addContactModalOpened) {
      setAddContactModalOpened(false);
      setData([...data, contact]);
    } else {
      setContactToBeChanged(null);
      setData(data.map((row) => (row.id === contact.id ? contact : row)));
    }
  };
  const removeRow = (id: number) => {
    setData(data.filter((row) => row.id !== id));
  };

  return (
    <div className="page">
      <EditContactModal
        contactToBeChanged={contactToBeChanged}
        close={() => setContactToBeChanged(null)}
        saveRow={saveRow}
      />
      <AddContactModal
        isOpened={addContactModalOpened}
        close={() => setAddContactModalOpened(false)}
        saveRow={saveRow}
      />
      <ContactList data={data} editRow={editRow} removeRow={removeRow} />
      <Button onClick={() => setAddContactModalOpened(true)}>
        Add contact
      </Button>
    </div>
  );
};

export default Contacts;
