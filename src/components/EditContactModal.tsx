import React from "react";
import "./Contacts.css";
import Modal from "./portals/modal/Modal";
import EditForm from "./EditForm";
import { ContactInterface } from "./ContactList";

interface Props {
  contactToBeChanged: ContactInterface | null;
  close: () => void;
  saveRow: (contact: ContactInterface) => void;
}

const EditContactModal = ({ contactToBeChanged, close, saveRow }: Props) => {
  if (!contactToBeChanged) return null;

  return (
    <Modal title="Edit Contact" close={close}>
      <EditForm contactToBeChanged={contactToBeChanged} onSubmit={saveRow} />
    </Modal>
  );
};

export default EditContactModal;
