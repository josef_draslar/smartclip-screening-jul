import React from "react";
import { render, screen } from "@testing-library/react";
import Button from "./Button";
import Form from "./Form";

test("form submit", () => {
  const onClickMock = jest.fn();
  render(
    <Form onSubmit={onClickMock}>
      <Button type="submit">test</Button>
    </Form>
  );
  const buttonElement = screen.getByText(/test/i);

  buttonElement.click();
  expect(onClickMock).toBeCalledTimes(1);
});
