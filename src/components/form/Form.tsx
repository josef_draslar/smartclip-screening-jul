import React from "react";
import "./Form.css";

interface Props {
  onSubmit: () => void;
  children: JSX.Element | JSX.Element[];
}

const Form = ({ onSubmit, children }: Props) => {
  return (
    <form onSubmit={onSubmit} className="form">
      {children}
    </form>
  );
};

export default Form;
