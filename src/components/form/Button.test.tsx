import React from "react";
import { render, screen } from "@testing-library/react";
import Button from "./Button";
import Form from "./Form";

test("disabled button with onClick", () => {
  const onClickMock = jest.fn();
  render(
    <Button disabled={true} onClick={onClickMock}>
      test
    </Button>
  );
  const buttonElement = screen.getByText(/test/i);
  buttonElement.click();
  expect(buttonElement).toBeInTheDocument();
  expect(onClickMock).not.toBeCalled();
});

test("button with onClick", () => {
  const onClickMock = jest.fn();
  render(<Button onClick={onClickMock}>test</Button>);
  const buttonElement = screen.getByText(/test/i);
  buttonElement.click();
  expect(buttonElement).toBeInTheDocument();
  expect(onClickMock).toBeCalledTimes(1);
});

test("button default type", () => {
  render(<Button>test</Button>);
  const buttonElement = screen.getByText(/test/i);

  expect(buttonElement.getAttribute("type")).toBe("button");
});

test("button props type", () => {
  const type = "submit";
  render(<Button type={type}>test</Button>);
  const buttonElement = screen.getByText(/test/i);

  expect(buttonElement.getAttribute("type")).toBe(type);
});
