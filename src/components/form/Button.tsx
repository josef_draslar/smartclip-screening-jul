import React from "react";
import "./Button.css";
import classNames from "classnames";

interface Props {
  children: string;
  disabled?: boolean;
  onClick?: () => void;
  type?: "button" | "submit" | "reset";
}

const Button = ({ onClick, children, disabled, type = "button" }: Props) => {
  return (
    <button
      className={classNames("button", { disabled: !!disabled })}
      type={type}
      onClick={disabled ? undefined : onClick}
    >
      {children}
    </button>
  );
};

export default Button;
