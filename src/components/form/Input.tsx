import React, { ForwardedRef } from "react";
import "./Input.css";

interface Props {
  name: string;
  value: string | number;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input = (
  { name, value, onChange }: Props,
  ref: ForwardedRef<HTMLInputElement>
) => {
  const id = "input-" + name;
  return (
    <div className="input-wrapper">
      <label htmlFor={id}>{name}</label>
      <input
        id={id}
        className="input"
        name={name}
        onChange={onChange}
        value={value}
        ref={ref}
      />
    </div>
  );
};

export default React.forwardRef(Input);
