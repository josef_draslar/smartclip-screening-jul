import React from "react";
import "./Contacts.css";
import Modal from "./portals/modal/Modal";
import EditForm from "./EditForm";
import { ContactInterface } from "./ContactList";

interface Props {
  isOpened: boolean;
  close: () => void;
  saveRow: (contact: ContactInterface) => void;
}

const AddContactModal = ({ isOpened, close, saveRow }: Props) => {
  if (!isOpened) return null;

  return (
    <Modal title="Add Contact" close={close}>
      <EditForm
        contactToBeChanged={{
          id: new Date().getTime(),
          name: "",
          phone: "",
        }}
        onSubmit={saveRow}
        focusFirstInputOnLoad
      />
    </Modal>
  );
};

export default AddContactModal;
