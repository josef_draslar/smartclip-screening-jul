import React from "react";

export const MODAL_PORTAL_DOM_ID = "ts-modal-portal";

const ModalPortal = () => {
  return <div id={MODAL_PORTAL_DOM_ID} />;
};

export default ModalPortal;
