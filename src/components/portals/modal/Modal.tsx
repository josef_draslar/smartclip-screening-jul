import React from "react";
import ReactDOM from "react-dom";
import { MODAL_PORTAL_DOM_ID } from "./ModalPortal";
import ModalStructure, { ModalStructureInterface } from "./ModalStructure";

const Modal = ({ children, title, close }: ModalStructureInterface) => {
  return ReactDOM.createPortal(
    <ModalStructure close={close} title={title}>
      {children}
    </ModalStructure>,
    document.getElementById(MODAL_PORTAL_DOM_ID) as HTMLElement
  );
};

export default Modal;
