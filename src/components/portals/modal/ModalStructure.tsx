import React, { ReactChild } from "react";
import "./ModalStructure.css";

export interface ModalStructureInterface {
  title: string;
  children: ReactChild;
  close: () => void;
}

const ModalStructure = ({
  title,
  children,
  close,
}: ModalStructureInterface) => {
  const onKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) => {
    if (e.code === "Escape") {
      close();
    }
  };

  return (
    <div className="wrapper">
      <div className="backdrop" onClick={close} />
      <div
        className="modal"
        onKeyDown={onKeyDown}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <svg
          className="modal-svg"
          xmlns="http://www.w3.org/2000/svg"
          width="100%"
          height="100%"
          preserveAspectRatio="none"
        >
          <rect
            x="0"
            y="0"
            fill="none"
            width="100%"
            height="100%"
            rx="3"
            ry="3"
          />
        </svg>
        <div>
          <div className="title-row">
            <h2>{title}</h2>
            <span onClick={close}>✕</span>
          </div>
          {children}
        </div>
      </div>
    </div>
  );
};

export default ModalStructure;
