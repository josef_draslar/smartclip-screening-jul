import React from "react";
import "./ContactList.css";

export interface ContactInterface {
  id: number;
  name: string;
  phone: string;
}
export interface Props {
  data: ContactInterface[];
  editRow: (contact: ContactInterface) => void;
  removeRow: (contactId: number) => void;
}

const ContactList = ({ data, editRow, removeRow }: Props) => {
  return (
    <div>
      <div className="table-header">
        <table cellPadding="0" cellSpacing="0">
          <thead>
            <tr>
              <th>Name</th>
              <th>Phone number</th>
              <th>Actions</th>
            </tr>
          </thead>
        </table>
      </div>
      <div className="table-content">
        <table cellPadding="0" cellSpacing="0">
          <tbody>
            {data.map((contact, index) => (
              <tr key={index}>
                <td>{contact.name}</td>
                <td>{contact.phone}</td>
                <td>
                  <span className="action" onClick={() => editRow(contact)}>
                    ✎
                  </span>
                  <span
                    className="action"
                    onClick={() => removeRow(contact.id)}
                  >
                    ✕
                  </span>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ContactList;
