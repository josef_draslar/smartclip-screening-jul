import React from "react";
import "./App.css";
import Contacts from "./components/Contacts";
import ModalPortal from "./components/portals/modal/ModalPortal";

function App() {
  return (
    <div className="app">
      <ModalPortal />
      <header className="app-header">
        <h1>Smartclip screening App</h1>
        <Contacts />
      </header>
    </div>
  );
}

export default App;
